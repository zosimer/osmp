 /*   
 * Project: OSMP
 * FileName: NettyServerChannelInitializer.java
 * version: V1.0
 */
package com.osmp.netty.server;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

/**
 * Description:
 * @author: wangkaiping
 * @date: 2017年2月7日 上午11:32:48上午10:51:30
 */
@Component
public class NettyServerChannelInitializer extends ChannelInitializer<SocketChannel>{
	
	@Resource
	private NettyServerDispatchHandler serverDispatchHandler;

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ch.pipeline().addLast(new ObjectEncoder());
		ch.pipeline().addLast(new ObjectDecoder(ClassResolvers.cacheDisabled(this.getClass().getClassLoader())));
		ch.pipeline().addLast(serverDispatchHandler);
		
	}

}
