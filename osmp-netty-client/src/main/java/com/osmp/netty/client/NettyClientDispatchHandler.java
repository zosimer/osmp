package com.osmp.netty.client;

import static java.util.concurrent.TimeUnit.SECONDS;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import org.springframework.stereotype.Component;

import com.osmp.intf.define.server.Request;
import com.osmp.intf.define.server.Response;
import com.osmp.intf.define.server.Server;

@Sharable
@Component
public class NettyClientDispatchHandler extends SimpleChannelInboundHandler<Response> {
	
	private final ConcurrentHashMap<String, BlockingQueue<Response>> responseMap = new ConcurrentHashMap<String, BlockingQueue<Response>>();
	
	@Override
	public void write(final ChannelHandlerContext ctx, final Object msg, final ChannelPromise promise) throws Exception {
		if (msg instanceof Request) {
			Request request = (Request) msg;
			responseMap.putIfAbsent(request.getMsgId(), new LinkedBlockingQueue<Response>(1));
		}
		super.write(ctx, msg, promise);
	}
	
	@Override
	protected void messageReceived(final ChannelHandlerContext ctx, final Response response) throws Exception {
		BlockingQueue<Response> queue = responseMap.get(response.getMsgId());
		queue.add(response);
	}
	
	public Response getResponse(final String messageId) {
		Response result;
		responseMap.putIfAbsent(messageId, new LinkedBlockingQueue<Response>(1));
		try {
			result = responseMap.get(messageId).take();
			if (null == result) {
				result = getSystemMessage();
			}
		} catch (final InterruptedException ex) {
			throw new RuntimeException(ex);
		} finally {
			responseMap.remove(messageId);
		}
		return result;
	}
	
	private Response getSystemMessage() {
		try {
			return responseMap.get(Server.SYSTEM_MESSAGE_ID).poll(5, SECONDS);
		} catch (final InterruptedException ex) {
			throw new RuntimeException(ex);
		}
	}
}
