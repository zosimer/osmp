/*   
 * Project: OSMP
 * FileName: LoginMapper.java
 * version: V1.0
 */
package com.osmp.web.panel.dao;

import com.osmp.web.core.mybatis.BaseMapper;
import com.osmp.web.user.entity.User;

public interface LoginMapper extends BaseMapper<User> {

}
