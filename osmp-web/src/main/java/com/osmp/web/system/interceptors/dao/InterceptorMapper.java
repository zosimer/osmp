package com.osmp.web.system.interceptors.dao;

import com.osmp.web.core.mybatis.BaseMapper;
import com.osmp.web.system.interceptors.entity.Interceptor;

public interface InterceptorMapper extends BaseMapper<Interceptor> {

}
